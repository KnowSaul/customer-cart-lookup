<?php
namespace Knowsaul\AdminCustomerQuoteLookUp\Controller\Adminhtml\Quote;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends Action
{
    const CUSTOMER_QUOTE_LOOKUP = 'Knowsaul_AdminCustomerQuoteLookUp::customerquotelookup';
    /**
     * @var PageFactory
     */
    private $resultPageFactory;

    /**
     * Constructor
     * @param Context        $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed(self::CUSTOMER_QUOTE_LOOKUP);
    }

    /**
     * Load the page defined in view/adminhtml/layout/customerquotelookup_quote_index.xml
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend(__('View Customer Cart'));
        return $resultPage;
    }
}
