<?php
namespace Knowsaul\AdminCustomerQuoteLookUp\Block;

use Magento\Checkout\Model\Session;
use Magento\Customer\Model\Context as CustomerContext;
use Magento\Framework\App\Http\Context as HttpContext;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\QuoteRepository;

class CustCartUniqueId extends Template
{
    protected $_checkoutSession;
    private HttpContext $httpContext;
    private ManagerInterface $messageManager;
    private QuoteRepository $quoteRepository;

    /**
     * @param Context $context
     * @param Session $checkoutSession
     * @param HttpContext $httpContext
     * @param QuoteRepository $quoteRepository
     * @param ManagerInterface $messageManager
     * @param array $data
     */
    public function __construct(
        Context     $context,
        Session     $checkoutSession,
        HttpContext $httpContext,
        QuoteRepository $quoteRepository,
        ManagerInterface $messageManager,
        array       $data = []
    ) {
        $this->_checkoutSession = $checkoutSession;
        parent::__construct($context, $data);
        $this->httpContext = $httpContext;
        $this->messageManager = $messageManager;
        $this->quoteRepository = $quoteRepository;
    }

    /**
     * Fetch Customer's Quote entity_id
     * @return int|string
     */
    public function getCustCartUniqueId()
    {
        $quoteId = $this->_checkoutSession->getQuoteId();
        return !empty($quoteId) ? $quoteId : '';
    }

    /**
     * Check if Customer is Logged In
     * @return bool
     */
    public function isCustomerLoggedIn() {
        return (bool)$this->httpContext->getValue(CustomerContext::CONTEXT_AUTH);
    }

    /**
     * Fetch Customer's Name
     * @return int|string
     */
    public function getCustFullName()
    {
        $quoteId = $this->_checkoutSession->getQuoteId();
        try {
            /** @var Quote $quote */
            $quote = $this->quoteRepository->get($quoteId);
            $customerName = __('%1 %2', [$quote->getCustomer()->getFirstname(), $quote->getCustomer()->getLastname()]);
        } catch (\Exception $e) {
            $this->_logger->warning(__('Could not load Quote data to fetch Customer Name.'));
            $customerName = 'Loyal Customer';
        }
        return $customerName;
    }
}
