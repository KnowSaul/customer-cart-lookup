<?php
namespace Knowsaul\AdminCustomerQuoteLookUp\Controller\Adminhtml\Quote;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\QuoteRepository;

class SearchView extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var QuoteRepository
     */
    protected $quoteRepository;

    /**
     * Constructor
     *
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param QuoteRepository $quoteRepository
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        QuoteRepository $quoteRepository
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->quoteRepository = $quoteRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $quoteId = (int)$this->getRequest()->getPostValue('quote_id');
        if (!$quoteId) {
            $this->messageManager->addError(__('Please enter a valid numeric Cart/Quote ID.'));
            return $this->resultRedirectFactory->create()->setPath('*/*/index');
        }
        try {
            /** @var Quote $quote */
            $quote = $this->quoteRepository->get($quoteId);
        } catch (\Exception $e) {
            $this->messageManager->addError(__('Could not load Cart/Quote data. Try again.'));
            return $this->resultRedirectFactory->create()->setPath('*/*/index');
        }
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend(__('View Customer Cart Search Result for Cart ID: [ %1 ]', $quoteId));
        $block = $resultPage->getLayout()->getBlock('knowsaul.admincustomerquotelookup.searchview');
        if ($block) {
            $block->setQuote($quote);
        }
        return $resultPage;
    }
}
