# Customer Cart Lookup



## Getting started

### Features
## 1 - 
This solution includes a Storefront Customer Cart Unique ID located at the HEADER_LINKS on every page right next to the Customer name section and only visible when the customer is LOGGEDIN. Which the customer upon calling customer service for support can reference such data so that technical Support may help by looking up the cart items in the admin panel. 

## 2 -

In the Admin panel dashboard, Tech Support will find under Customers Menu a new option called **View Customer Cart**
where a new input page opens up.

## 3 -

Once within **View Customer Cart** support has the option to request the Customer's unique Cart ID from the storefront and input that in the form field and submit. The form validates for the required input field and enforces a integer type.

## 4 - 
Upon successfully submitting the form, if the system finds no matching or empty Cart, there will be a graceful error message.
PS: If this happens, refer to the customer and make sure they still have an active cart and that their cart has not been cleared recently.

## 5 -
If a match is found between the Quote_ID provided from the storefront and an Active Quote in Magento, then a grid of the Customer's Cart will display including a separation between Pruduct Type and their Options.
Details you will find:
Product Name, SKU, Type, Options, Price at the line item, and Cart total.
