<?php
namespace Knowsaul\AdminCustomerQuoteLookUp\Block\Adminhtml;

use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Magento\Bundle\Helper\Catalog\Product\Configuration;
use Magento\Bundle\Model\Product\Type;
use Magento\Catalog\Model\Product;
use Magento\Customer\Api\GroupRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Pricing\Helper\Data as PricingData;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Quote\Model\Quote\Item;
use Magento\Catalog\Model\Product\Type as ProductType;

class SearchView extends Template
{
    private SerializerInterface $serializer;
    private PricingData $pricingData;
    private Configuration $configuration;
    private PricingData $formatPrice;
    private SearchCriteriaBuilder $searchCriteriaBuilder;
    private GroupRepositoryInterface $customerGroupRepository;

    /**
     * @param Context $context
     * @param SerializerInterface $serializer
     * @param PricingData $pricingData
     * @param Configuration $configuration
     * @param PricingData $formatPrice
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param GroupRepositoryInterface $customerGroupRepository
     * @param array $data
     */
    public function __construct(
        Context             $context,
        SerializerInterface $serializer,
        PricingData         $pricingData,
        Configuration       $configuration,
        PricingData         $formatPrice,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        GroupRepositoryInterface $customerGroupRepository,
        array               $data = []
    )
    {
        parent::__construct($context, $data);
        $this->serializer = $serializer;
        $this->pricingData = $pricingData;
        $this->configuration = $configuration;
        $this->formatPrice = $formatPrice;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->customerGroupRepository = $customerGroupRepository;
    }

    /**
     * Get item configurable child product options
     */
    public function getChildProductOptions($item)
    {
        $optionResult = [];
        $options = $item->getProduct()->getTypeInstance(true)->getSelectedAttributesInfo($item->getProduct());
        foreach ($options as $option) {
            $optionResult[$option['label']] = $option['value'];
        }
        return $optionResult;
    }

    /**
     * Fetches Bundle Product Options
     * @param $item
     * @return array
     */
    public function getBundleOptions($item)
    {
        $options = [];
        $product = $item->getProduct();
        $optionsQuoteItemOption = $item->getOptionByCode('bundle_option_ids');
        $bundleOptionsIds = $optionsQuoteItemOption
            ? $this->serializer->unserialize($optionsQuoteItemOption->getValue())
            : [];
        /** @var Type $typeInstance */
        $typeInstance = $product->getTypeInstance();
        if ($bundleOptionsIds) {
            $selectionsQuoteItemOption = $item->getOptionByCode('bundle_selection_ids');
            $optionsCollection = $typeInstance->getOptionsByIds($bundleOptionsIds, $product);
            $bundleSelectionIds = $this->serializer->unserialize($selectionsQuoteItemOption->getValue());
            if (!empty($bundleSelectionIds)) {
                $selectionsCollection = $typeInstance->getSelectionsByIds($bundleSelectionIds, $product);
                $bundleOptions = $optionsCollection->appendSelections($selectionsCollection, true);
                $options = $this->buildBundleOptions($bundleOptions, $item);
            }
        }
        $bundleOptionResult = [];
        foreach ($options as $parentKey => $optionArray) {
            foreach ($optionArray['values'] as $bundleOption) {
                foreach ($bundleOption as $chilKey => $finalResult) {
                    if ($chilKey != 'id' && $chilKey != 'price') {
                        $bundleOptionResult[$parentKey][$chilKey] = $finalResult;
                    }
                    if ($chilKey == 'price') {
                        $bundleOptionResult[$parentKey][$chilKey] = $this->formatToUsdCurrency($finalResult);
                    }
                }
            }
        }
        return $bundleOptionResult;
    }

    /**
     * Format number to USD Currency
     * @param $number
     * @return float|string
     */
    public function formatToUsdCurrency($number) {
        return $this->formatPrice->currency($number, true, false);
    }
    /**
     * Build bundle product options based on current selection
     * @param array $bundleOptions
     * @param Item $item
     * @return array
     */
    private function buildBundleOptions(array $bundleOptions, Item $item): array
    {
        $options = [];
        foreach ($bundleOptions as $bundleOption) {
            if (!$bundleOption->getSelections()) {
                continue;
            }
            $options[] = [
                'id' => $bundleOption->getId(),
                'label' => $bundleOption->getTitle(),
                'type' => $bundleOption->getType(),
                'values' => $this->buildBundleOptionValues($bundleOption->getSelections(), $item),
            ];
        }
        return $options;
    }

    /**
     * Build bundle product option values based on current selection
     * @param array $selections
     * @param Item $item
     * @return array
     */
    private function buildBundleOptionValues(array $selections, Item $item): array
    {
        $values = [];

        $product = $item->getProduct();
        foreach ($selections as $selection) {
            $qty = (float) $this->configuration->getSelectionQty($product, $selection->getSelectionId());
            if (!$qty) {
                continue;
            }

            $selectionPrice = $this->configuration->getSelectionFinalPrice($item, $selection);
            $optionDetails = [
                ProductType::TYPE_BUNDLE,
                $selection->getData('option_id'),
                $selection->getData('selection_id'),
                (int) $selection->getData('selection_qty')
            ];
            $values[] = [
                'id' => $selection->getSelectionId(),
                'label' => $selection->getName(),
                'quantity' => $qty,
                'price' => $this->pricingData->currency($selectionPrice, false, false),
            ];
        }
//        $valueOptions = implode('<br>', $values);
        return $values;
    }

    /**
     * Fetches the Customer Group Code name by Group ID from Quote
     * @param string $groupId
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function lookUpCustomerGroupById(string $groupId) {
        $searchCriteria = $this->searchCriteriaBuilder->addFilter('customer_group_id',$groupId,'eq')->create();
        $customerGroupRepositoryResult  = $this->customerGroupRepository->getList($searchCriteria)->getItems();
        foreach ($customerGroupRepositoryResult as $customerGroupData) {
            $customerGroupCode = $customerGroupData->getCode();
        }
        return $customerGroupCode;
    }
}
